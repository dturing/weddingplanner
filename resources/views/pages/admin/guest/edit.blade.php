@extends('layouts.app')
@section('title', 'Edit Guest')

@section('content')
    <h1>Update Guest Details</h1>

    {!! Form::open(['action' => ['Admin\UserController@update', $wedding->id, $user->id], 'method' => 'PUT']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'John Smith', 'required'])}}
        </div>

        <div class="form-group">
                {{Form::label('email', 'Email')}}
                {{Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => 'example@example.com', 'required'])}}
            </div>

        <h5>Wedding Options</h5>
        <h6>The options below cannot be configured by the guest, you should set them here.</h6>
        <br/>

        <div class="form-group">
            {{Form::checkbox('organiser', null, $user->hasRole('Organiser'))}}
            {{Form::label('organiser', 'Organiser')}}
        </div>

        <div class="form-group">
            {{Form::checkbox('allowed_chaperon', null, $user->allowed_chaperon)}}
            {{Form::label('allowed_chaperon', 'Allow Plus One')}}
        </div>

        <h5>Manual Options</h5>
        <h6>The options below can be configured by the guest when they login, if you wish to manually set a setting it can be done here.</h6>
        <br/>

        <div class="form-group">
            {{Form::checkbox('attending', null, $user->attending)}}
            {{Form::label('attending', 'Attending')}}
        </div>

        @if ($wedding->accommodation_available)
            <div class="form-group">
                    {{Form::checkbox('wants_accommodation', null, $user->wants_accommodation)}}
                    {{Form::label('wants_accommodation', 'Consider For Accommodation')}}
            </div>
        @endif

        {{Form::hidden('wedding_id', $wedding->id)}}
        {{Form::hidden('user_id', $user->id)}}
        {{Form::submit('Update Guest', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}/user/{{$user->slug}}" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection