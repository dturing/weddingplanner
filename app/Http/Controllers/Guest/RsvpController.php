<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Wedding;
use App\User;

class RsvpController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser|Guest');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  User     $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, User $user)
    {
        debug('[RSVP] Updating attending for user.');
        
        $user->attending = ($request->attending != null);
        $user->save();

        $message = 'Welcome to the party pal!';
        
        if (!$user->attending) {
            $message = "You're dead to us!";

            $user->menu_choice = null;
            $user->menu_comment = null;

            $user->save();
        }

        session()->flash('success', $message);
        
        return redirect()->action('Guest\MenuController@index', [$wedding]);
    }
}
