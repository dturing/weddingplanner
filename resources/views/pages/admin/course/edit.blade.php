@extends('layouts.app')
@section('title', 'Edit Course')

@section('content')
    <h1>Edit Course</h1>
    
    {!! Form::open(['action' => ['Admin\CourseController@update', $wedding->id, $menu->id, $course->id], 'method' => 'PUT']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $course->name, ['class' => 'form-control', 'placeholder' => 'Normal', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', $course->description, ['class' => 'form-control', 'placeholder' => 'The default menu with no special dietary requirements.', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('position', 'Position (What course is this? 1st = 1, 2nd = 2, etc...)')}}
            {{Form::number('position', $course->position, ['class' => 'form-control', 'placeholder' => 'The default menu with no special dietary requirements.', 'required'])}}
        </div>

        {{Form::hidden('course_id', $course->id)}}
        {{Form::submit('Update Course', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course/{{$course->slug}}" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection