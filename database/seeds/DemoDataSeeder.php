<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DemoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a wedding (will get the ID of 1)
        DB::table('weddings')->insert([
            'name'                    => 'Wedding 1',
            'slug'                    => 'wedding-1',
            'when'                    => '2019-12-10',
            'accommodation_available' => true,
            'created_at'              => Carbon::now(),
            'updated_at'              => Carbon::now()
        ]);

        // Create a user (will get the ID of 2)
        DB::table('users')->insert(
            array(
                array( // Gets the ID of 2
                    'name'       => 'Test Man',
                    'slug'       => 'test-man',
                    'email'      => 'test@test.com',
                    'password'   => Hash::make('test'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array( // Gets the ID of 3
                    'name'       => 'Organiser Human',
                    'slug'       => 'organiser-human',
                    'email'      => 'organiser@test.com',
                    'password'   => Hash::make('organiser'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                )
            )
        );

        // Assign the role of 'Guest' to User ID 2
        DB::table('user_role')->insert(
            array(
                array(
                    'user_id' => 2,
                    'role_id' => 3,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'user_id' => 3,
                    'role_id' => 2,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                )
            )
        );

        // Assign Wedding ID 1 to User ID 2
        DB::table('user_wedding')->insert(
            array(
                array(
                    'user_id'    => 2,
                    'wedding_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'user_id'    => 3,
                    'wedding_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                )
            )
        );

        // Create a Menu and assign to Wedding ID 1
        DB::table('menus')->insert(
            array(
                array( // Gets the ID of 1
                    'wedding_id'  => 1,
                    'name'        => 'Vegetarian',
                    'slug'        => 'vegetarian',
                    'description' => 'This is the vegetarian menu.',
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now()
                ),
                array( // Gets the ID of 2
                    'wedding_id'  => 1,
                    'name'        => 'Meaty',
                    'slug'        => 'meaty',
                    'description' => 'This is the meaty menu.',
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now()
                )
            )
        );

        // Create courses for menu
        DB::table('courses')->insert(
            array(
                array( // Gets the ID of 1
                    'menu_id'     => 1,
                    'position'    => 1,
                    'name'        => 'Starter',
                    'slug'        => 'starter',
                    'description' => 'This is the starter for vegetarian.',
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now() 
                ),
                array( // Gets the ID of 2
                    'menu_id'     => 2,
                    'position'    => 1,
                    'name'        => 'Starter',
                    'slug'        => 'starter-1',
                    'description' => 'This is the starter for meaty.',
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now()
                )
            )
        );

        // Create food for the courses
        DB::table('foods')->insert(
            array(
                array(
                    'course_id'      => 1,
                    'name'           => 'Cardboard',
                    'slug'           => 'cardboard',
                    'description'    => 'This is cardboard.',
                    'vegan'          => true,
                    'vegetarian'     => true,
                    'contains_nuts'  => false,
                    'contains_dairy' => false,
                    'gluten_free'    => false,
                    'created_at'     => Carbon::now(),
                    'updated_at'     => Carbon::now()
                ),
                array(
                    'course_id'      => 2,
                    'name'           => 'Bacon',
                    'slug'           => 'bacon',
                    'description'    => 'This is Bacon.',
                    'vegan'          => true,
                    'vegetarian'     => true,
                    'contains_nuts'  => false,
                    'contains_dairy' => false,
                    'gluten_free'    => false,
                    'created_at'     => Carbon::now(),
                    'updated_at'     => Carbon::now()
                )
            )
        );
    }
}
