<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Wedding;
use App\Menu;


class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function index(Wedding $wedding)
    {
        return view('pages.admin.menu.index')
                ->with(compact('wedding'))
                ->with('menus', $wedding->menus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function create(Wedding $wedding)
    {
        return view('pages.admin.menu.create')
                ->with(compact('wedding'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Wedding $wedding)
    {
        $this->validate(request(), [
            'wedding_id'  => 'required',
            'name'        => 'required|min:1|max:100',
            'description' => 'required|min:1|max:100',
        ]);

        $menu = $wedding->upsertMenu($request);

        session()->flash('success', 'Wedding Menu "' . $menu->name . '" Created');
        
        return redirect()->action('Admin\MenuController@index', [$wedding])
                ->with('menus', $wedding->menus);
    }

    /**
     * Display the specified resource.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Wedding $wedding, Menu $menu)
    {
        return view('pages.admin.menu.show')
                ->with(compact('wedding'))
                ->with(compact('menu'))
                ->with('courses', $menu->courses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menuId
     * @return \Illuminate\Http\Response
     */
    public function edit(Wedding $wedding, Menu $menu)
    {
        return view('pages.admin.menu.edit')
                ->with(compact('wedding'))
                ->with(compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, Menu $menu)
    {
        $this->validate(request(), [
            'wedding_id'  => 'required',
            'name'        => 'required|min:1|max:100',
            'description' => 'required|min:1|max:100',
        ]);
        
        $menu = $wedding->upsertMenu($request);

        session()->flash('success', 'Updated Menu "' . $menu->name . '" details');

        return redirect()->action('Admin\MenuController@show', [$wedding, $menu])
                ->with('courses',$menu->courses);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wedding $wedding, Menu $menu)
    {
        $users = $wedding->users()->where('menu_choice', $menu->id);

        foreach($users as $user) {
            $user->menu_choice = null;
            $user->menu_comment = null;
            $user->save();
        }

        $menu->delete();

        session()->flash('success', 'Removed menu "' . $menu->name . '"');

        return redirect()->action('Admin\MenuController@index', [$wedding])
                ->with('menus', $wedding->menus);
    }
}
