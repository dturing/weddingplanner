@extends('layouts.app')
@section('title', 'View Wedding')

@section('content')
    @include('inc.nav.weddingManagement')
    <div class="card">
        <div class="card-body">
            <h3 class="card-title">Wedding Details<h3>
            <h6><strong>Name:</strong> {{$wedding->name}}</h6>
            <h6><strong>Date:</strong> {{$wedding->when}}</h6>
            <h6><strong>Accommodation Available:</strong> {{$wedding->hasAccommodation()}} ({{count($wedding->wantsAccommodation())}} for accommodation)
            <h6><strong>RSVPs:</strong>
                <ul>
                    <li>Yes: {{count($wedding->attending())}}</li>
                    <li>No: {{count($wedding->notAttending())}}</li>
                    <li>Not Responded: {{count($wedding->noRsvp())}}</li>
                </ul>
            </h6>
            <h6><strong>Menu Choices</strong>
                @if(count($wedding->menus) > 0)
                    <ul>
                        @foreach($wedding->menus as $menu)
                            <li>{{$menu->name}}: {{count($menu->chosen())}}</li>
                        @endforeach

                        @if(count($wedding->noMenuChosen()) > 0)
                            <li>{{count($wedding->noMenuChosen())}} guests have not chosen a menu.</li>
                        @endif
                    </ul>
                @else
                    <p>No menus have been configured.</p>
                @endif
            </h6>
        </div>
    </div>
    <br/>
    
    <a href="/admin/wedding/{{$wedding->slug}}/edit" class="btn btn-primary">Edit</a>

    @if (Auth::user()->isSuperUser())
        {!! Form::open(['action' => ['Admin\WeddingController@destroy', $wedding->id], 'method' => 'POST']) !!}
            
            {{ Form::hidden('_method', 'DELETE') }}

            <br/>
            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}

            <a href="/admin/wedding" class="btn btn-error float-right">Go Back</a>
        {!! Form::close() !!}
    @endif
@endsection