@extends('layouts.app')
@section('title', 'Create Dish')

@section('content')
    <h1>Add a Food</h1>

    {!! Form::open(['action' => ['Admin\FoodController@store', $wedding->id, $menu->id, $course->id], 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name of the dish', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', '', ['class' => 'form-control', 'placeholder' => 'A tasty treat for anyone.', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('vegetarian', 'Vegetarian')}}
            {{Form::checkbox('vegetarian')}}
        </div>

        <div class="form-group">
            {{Form::label('vegan', 'Vegan')}}
            {{Form::checkbox('vegan')}}
        </div>

        <div class="form-group">
            {{Form::label('contains_nuts', 'Contains Nuts')}}
            {{Form::checkbox('contains_nuts')}}
        </div>

        <div class="form-group">
            {{Form::label('contains_dairy', 'Contains Dairy')}}
            {{Form::checkbox('contains_dairy')}}
        </div>

        <div class="form-group">
            {{Form::label('gluten_free', 'Gluten Free')}}
            {{Form::checkbox('gluten_free')}}
        </div>

        {{Form::submit('Add Food To Course', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course/{{$course->slug}}" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection