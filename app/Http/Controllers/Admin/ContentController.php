<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Wedding;
use App\Content;

class ContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Wedding $wedding)
    {
        $contents = $wedding->contents;

        return view('pages.admin.content.index')
                ->with(compact('wedding'))
                ->with(compact('contents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Wedding $wedding)
    {
        return view('pages.admin.content.create')
                ->with(compact('wedding'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Wedding $wedding)
    {
        $this->validate(request(), [
            'name'        => 'required|min:1|max:100',
            'description' => 'required|min:1|max:100',
            'content'     => 'required|min:1'
        ]);

        $content = new Content;
        $content->wedding_id = $wedding->id;
        $content->name = $request->name;
        $content->description = $request->description;
        $content->content = $request->content;
        $content->save();

        session()->flash('success', 'Successfully created "' . $content->name . '" page!');

        return redirect()->action('Admin\ContentController@index', [$wedding])
            ->with('contents', $wedding->contents);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Wedding $wedding, Content $content)
    {
        return view('pages.admin.content.show')
                ->with(compact('wedding'))
                ->with(compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Wedding $wedding, Content $content)
    {
        return view('pages.admin.content.edit')
                ->with(compact('wedding'))
                ->with(compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, Content $content)
    {
        $this->validate(request(), [
            'name'        => 'required|min:1|max:100',
            'description' => 'required|min:1|max:100',
            'content'     => 'required|min:1'
        ]);

        $content->name = $request->name;
        $content->description = $request->description;
        $content->content = $request->content;
        $content->save();

        session()->flash('success', 'Wedding content "' . $content->name . '" updated!');

        return redirect()->action('Admin\ContentController@show', [$wedding, $content]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Wedding  $wedding
     * @param  Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wedding $wedding, Content $content)
    {
        $content->delete();

        session()->flash('success', 'Succesfully removed the "' . $content->name . '" page');

        return redirect()->action('Admin\ContentController@index', [$wedding])
                ->with('contents', $wedding->contents);                
    }
}
