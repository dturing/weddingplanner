@extends('layouts.app')
@section('title', 'Courses')

@section('content')
    @include('inc.nav.weddingManagement')

    <h2>Menu Courses</h2>

    @if(count($courses) > 0)
        @foreach($courses as $course)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course/{{$course->slug}}">{{$course->name}}</a></h5>
                    <p>{{$course->description}}</p>
                </div>
            </div>
            <br/>
        @endforeach
    @else
        <p>No course are configured for this menu.</p>
    @endif

        <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course/create" class="btn btn-primary">Create Course</a>
    <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}" class="btn btn-error float-right">Go Back</a>
@endsection