#! /usr/bin/env bash

echo ============================================
echo Starting Wedding Panel Initialisation Script
echo ============================================

echo == Moving Over To /var/www
cd /var/www

echo == [Artisan] Clearing Everything
php artisan clear-compiled

echo == [Composer] Clearing Everything
composer dump-autoload

echo == Updating Composer Packages
composer update

echo == Generating Encryption Key
php artisan key:generate

echo == Performing Initial Migration
php artisan migrate

echo == All Finished and ready to go!