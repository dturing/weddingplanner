<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserChaperon extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'menu_choice',
        'menu_comment',
        'wants_accommodation'
    ];

    /**
     * Each Chaperon is belongs to a guest.
     */
    public function guest() {
        return $this->belongsTo(User::class);
    }

    /**
     * Would the Chaperon like accommodation?
     */
    public function wantsAccommodation() {
        return isset($this->wants_accommodation)
            ? $this->wants_accommodation ? 'Yes' : 'No'
            : null;
    }
}
