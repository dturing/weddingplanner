@extends('layouts.app')
@section('title', 'Pages')

@section('content')
    @include('inc.nav.weddingManagement')

    <h2>Manage Website Content</h2>

    @if(count($contents) > 0)
        @foreach($contents as $content)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><strong>Name:</strong> <a href="/admin/wedding/{{$wedding->slug}}/content/{{$content->slug}}">{{$content->name}}</a></h5>
                    <p><strong>Slug:</strong> {{$content->slug}}</p>
                    <p><strong>Description:</strong> {{$content->description}}</p>
                </div>
            </div>
            <br/>
        @endforeach
    @else
        <p>You have not created any content yet!</p>
    @endif

    <a href="/admin/wedding/{{$wedding->slug}}/content/create" class="btn btn-primary">Create Content</a>
    <a href="/admin/wedding/{{$wedding->slug}}" class="btn btn-error float-right">Go Back</a>
@endsection