@extends('layouts.app')
@section('title', 'Edit Wedding')

@section('content')
    <h1>Create New Wedding</h1>
    
    {!! Form::open(['action' => ['Admin\WeddingController@update', $wedding->id], 'method' => 'put']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $wedding->name, ['class' => 'form-control', 'placeholder' => 'Our Special Day', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('when', 'When')}}
            {{Form::text('when', date('Y-m-d', strtotime($wedding->when)), ['class' => 'form-control', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::checkbox('accommodation_available', null, $wedding->accommodation_available)}}
            {{Form::label('accommodation', 'Is arranged accommodation an option for your guests?')}}
        </div>

        {{Form::hidden('wedding_id', $wedding->id)}}
        {{Form::submit('Update Wedding', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection