@extends('layouts.app')
@section('title', 'Weddings')

@section('content')
    <h1>Current Weddings</h1>

    @if (count($weddings) > 0)
        @foreach ($weddings as $wedding)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><a href="/admin/wedding/{{$wedding->slug}}">{{$wedding->name}}</a></h5>
                    <h6 class="card-subtitle mb-2 text-muted">Date: {{$wedding->when}}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">Guests: {{count($wedding->users())}}
                </div>
            </div>
            <br/>
        @endforeach
    @else
        <p>No weddings found.</p>
    @endif

    <a href="/admin/wedding/create" class="btn btn-primary">Create Wedding</a>
    <a href="/admin" class="btn btn-error float-right">Go Back</a>
@endsection