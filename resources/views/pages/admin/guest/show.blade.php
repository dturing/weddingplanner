@extends('layouts.app')
@section('title', 'View Guest')

@section('content')
    @include('inc.nav.weddingManagement')
    <h1>Guest Overview</h1>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{$user->name}}</a></h5>
            <div id="guest-email" class="guest-attribute"><strong>Email:</strong> {{$user->email}}</div>
            <div id="guest-role" class="guest-attribute"><strong>Roles:</strong></div>
            <ul>
                @foreach ($user->roles as $role)
                    <li>{{$role->name}}</li>
                @endforeach
            </ul>

            @if ($wedding->accommodation_available)
                <div id="guest-wants-accommdation" class="guest-attribute"><strong>Wants Accommodation:</strong> {{$user->wantsAccommodation()}}</div>
            @endif
            
            <div id="guest-attending" class="guest-attribute"><strong>Attending:</strong> {{$user->isAttending() }}</div>

            <div id="guest-menu-choice" class="guest-attribute"><strong>Menu Choice:</strong> {{$user->menuChoice()}}</div>

            @if($user->allowed_chaperon)
                <div id="guest-allowed-plus-one" class="guest-attribute"><strong>Allowed Plus One:</strong> {{$user->allowed_plus_one}}</div>
                <div class="card-body">
                    <h5 class="card-title">Plus One</h5>
                    @if($user->chaperon() != null)
                        <p>{{$user->chaperon()->name}}</p>
                    @else
                        <p>Guest has not chosen a plus one.</p>
                    @endif
                </div>
            @else
                <div id="guest-allowed-plus-one" class="guest-attribute"><strong>Chosen Plus One:</strong> Not allowed a plus one</div>
            @endif
        </div>
    </div>
    <br/>
    {!! Form::open(['action' => ['Admin\UserController@destroy', $wedding->slug, $user->slug], 'method' => 'POST']) !!}
        <a href="/admin/wedding/{{$wedding->slug}}/user/{{$user->slug}}/edit" class="btn btn-primary">Edit Guest</a>

        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Remove Guest', ['class' => 'btn btn-danger']) }}

        <a href="/admin/wedding/{{$wedding->slug}}/user" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection