<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Wedding;
use App\Menu;
use App\Course;

class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @return \Illuminate\Http\Response
     */
    public function index(Wedding $wedding, Menu $menu)
    {
        return view('pages.admin.course.index')
                ->with(compact('wedding'))
                ->with(compact('menu'))
                ->with('courses', $menu->courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Wedding $wedding
     * @param  Menu    $menu
     * @return \Illuminate\Http\Response
     */
    public function create(Wedding $wedding, Menu $menu)
    {
        return view('pages.admin.course.create')
                ->with(compact('wedding'))
                ->with(compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Wedding $wedding, Menu $menu)
    {
        $this->validate(request(), [
            'name'        => 'required|min:1|max:100',
            'description' => 'required|min:1|max:100',
            'position'    => 'required|Integer|min:1'
        ]);

        $course = $menu->upsertCourse($request);
        
        session()->flash('success', 'Added course "' . $course->name . '" to the "' . $menu->name . '" menu');
                
        return redirect()->action('Admin\MenuController@show', [$wedding, $menu])
                ->with('courses', $menu->courses);
    }

    /**
     * Display the specified resource.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @param  Course   $course
     * @return \Illuminate\Http\Response
     */
    public function show(Wedding $wedding, Menu $menu, Course $course)
    {
        return view('pages.admin.course.show')
                ->with(compact('wedding'))
                ->with(compact('menu'))
                ->with(compact('course'))
                ->with('foods', $course->foods);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menu 
     * @param  Course   $menuCourse
     * @return \Illuminate\Http\Response
     */
    public function edit(Wedding $wedding, Menu $menu, Course $course)
    {
        return view('pages.admin.course.edit')
                ->with(compact('wedding'))
                ->with(compact('menu'))
                ->with(compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @param  Course   $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, Menu $menu, Course $course)
    {
        $this->validate(request(), [
            'name'        => 'required|min:1|max:100',
            'description' => 'required|min:1|max:100',
            'position'    => 'required|Integer|min:1'
        ]);

        $course = $menu->upsertCourse($request);

        session()->flash('success', 'Updated course "' . $course->name . '" details.');

        return redirect()->action('Admin\CourseController@show', [$wedding, $menu, $course])
                ->with('foods', $course->foods);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @param  Course   $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wedding $wedding, Menu $menu, Course $course)
    {
        $course->delete();

        session()->flash('success', 'success', 'Removed course "' . $course->name . '" from the "' . $menu->name . '" menu');

        return redirect()->action('Admin\CourseController@index', [$wedding, $menu])
                ->with('courses', $menu->courses);
    }
}
