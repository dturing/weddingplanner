@extends('layouts.app')
@section('title', 'Add Guest')

@section('content')
    <h1>Create New Wedding Guest</h1>

    {!! Form::open(['action' => ['Admin\UserController@store', $wedding->id], 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'John Smith', 'required'])}}
        </div>

        <div class="form-group">
                {{Form::label('email', 'Email')}}
                {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'example@example.com', 'required'])}}
            </div>

        <div class="form-group">
            {{Form::label('password', 'Password')}}
            {{Form::password('password', ['class' => 'form-control', 'required'])}}
        </div>

        <h5>Wedding Options</h5>
        <h6>The options below cannot be configured by the guest, you should set them here.</h6>
        <br/>

        <div class="form-group">
            {{Form::checkbox('organiser', null)}}
            {{Form::label('organiser', 'Organiser')}}
        </div>

        <div class="form-group">
            {{Form::checkbox('allowed_chaperon')}}
            {{Form::label('allowed_chaperon', 'Allow Plus One')}}
        </div>

        <h5>Manual Options</h5>
        <h6>The options below can be configured by the guest when they login, if you wish to manually set a setting it can be done here.</h6>
        <br/>

        <div class="form-group">
            {{Form::checkbox('attending', null)}}
            {{Form::label('attending', 'Attending')}}
        </div>

        @if ($wedding->accommodation_available)
            <div class="form-group">
                    {{Form::checkbox('wants_accommodation', null)}}
                    {{Form::label('wants_accommodation', 'Consider For Accommodation')}}
            </div>
        @endif

        {{Form::hidden('wedding_id', $wedding->id)}}
        {{Form::submit('Add Guest', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}/user" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection