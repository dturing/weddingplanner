<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Food extends Model
{
    use HasSlug;

    public $timestamps = true;

    protected $fillable = [
        'course_id',
        'name',
        'slug',
        'description',
        'vegan',
        'vegetarian',
        'contains_nuts',
        'contains_dairy',
        'gluten_free'
    ];

    /**
     * Create slug automatically.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Use either slug or ID to find the record.
     */
    public function getRouteKeyName(): string
    {
        $identifier = Route::current()->parameters()['food'];

        if (!ctype_digit($identifier)) {
            return 'slug';
        }

        return 'id';
    }

    // Relationships     
    // Each Food is owned by a Course.
    public function course() {
        return $this->belongsTo(Course::class);
    }
}
