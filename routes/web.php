<?php
# HANDLE ALL VISITOR ROUTES (NOT LOGGED IN)
Route::get('/', 'Visitor\VisitorController@index');

#HANDLE ALL GUEST ROUTES (LOGGED IN AS A GUEST)
Route::get('/home', 'Guest\PageController@index');

# GUEST HANDLING
Route::prefix('{wedding}')->group(function() {
    Route::put('rsvp/{user}', 'Guest\RsvpController@update');

    Route::get('accommodation', 'Guest\AccommodationController@index');
    Route::put('accommodation/{user}','Guest\AccommodationController@update');

    Route::get('rsvp', 'Guest\MenuController@index');
    Route::put('menu/{user}', 'Guest\MenuController@update');

    Route::get('chaperon', 'Guest\ChaperonRsvpController@index');
    Route::put('chaperon/{user}', 'Guest\ChaperonRsvpController@update');
    Route::post('chaperon/{user}', 'Guest\ChaperonRsvpController@create');

    Route::get('page/{slug}', 'Guest\CustomController@index');
});

# HANDLE ALL ADMIN ROUTES (LOGGED IN AS SUPERUSER/ORGANISER)
Route::prefix('admin')->group(function() {
    Route::get('/', 'Admin\AdminController@index');

    Route::resource('wedding', 'Admin\WeddingController');

    Route::resource('wedding/{wedding}/content', 'Admin\ContentController');
    
    Route::resource('wedding/{wedding}/user', 'Admin\UserController');

    Route::resource('wedding/{wedding}/menu', 'Admin\MenuController');

    Route::resource('wedding/{wedding}/menu/{menu}/course', 'Admin\CourseController');

    Route::resource('wedding/{wedding}/menu/{menu}/course/{course}/food', 'Admin\FoodController');
});

Auth::routes();