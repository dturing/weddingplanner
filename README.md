## Prerequisites

- [Vagrant](https://www.vagrantup.com/)
- [VirtualBox](https://www.virtualbox.org/)

## Getting Up & Running

**Environment File**
1. Copy the **.env.example** to **.env** (**Note:** On Window platforms name the file ".env." as it won't accept a filename starting with a period)

**Setting Up ScotchBox & Configuration**
1. Open a Terminal
2. Navigate to the root of the project directory
3. Run ```vagrant up``` and wait for completion
4. The provision script **initialise.sh** will perform the following steps:
    * Make sure everything Artisan and Composer is cleaned and starting fresh (more for re-provisioning than initial)
    * Updating Composer packages
    * Generate the Laravel encryption key
    * Perform the first migration to get your database up-to-date
5. Optional: If you wish to seed the database with demo data, you can run ```php artisan db:seed```, this creates the following:
    * A wedding with 2 menus, each with course, and food.
    * A guest ```Email: test@test.com | Password: test```
    * An Organiser ```Email: organiser@test.com | Password: test```
6. The site is now available from **192.168.33.10**
7. You can login as site admin with:
    * Email: admin@admin.com
    * Password: admin

## Issues & Fixes
**Issue** - On occassion after starting up the box, you can may an error when accessing the route website (forbidden).

**Fix**   - With the Vagrant machine running, run the command ```vagrant reload```.