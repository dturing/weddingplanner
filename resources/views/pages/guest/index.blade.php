@extends('layouts.app')
@section('title', 'Welcome')

@section('content')
    <div class="jumbotron text-center">
        <h1>{{$wedding->name}}</h1>
        <h4>{{$wedding->when}}</h4>
        <p>Welcome to the wedding {{Auth::User()->name}}!</p>
    </div>
@endsection	