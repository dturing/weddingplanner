<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Wedding;
use App\User;
use App\UserChaperon;

class ChaperonRsvpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Wedding $wedding)
    {
        $user = auth()->user();

        return view('pages.guest.chaperon')
                ->with(compact('wedding'))
                ->with(compact('user'))
                ->with('chaperon', $user->chaperon());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Wedding $wedding, User $user)
    {
        $this->validate(request(), [
            'name'  => 'required|min:1|max:100',
            'email' => 'required|email|min:1|max:100',
        ]);

        $chaperon = $user->chaperon();

        debug(isset($chaperon) 
            ? '[Chaperon] Updating a Chaperon.'
            : '[Chaperon] Creating a Chaperon.');

        if (isset($chaperon))
        {
            $chaperon->name = $request->name;
            $chaperon->email = $request->email;
            $chaperon->menu_choice = isset($request->menu_choice) ? $request->menu_choice : null;
            $chaperon->menu_comment = isset($request->menu_comment) ? $request->menu_comment : null;
            $chaperon->wants_accommodation = isset($request->wants_accommodation);

            $chaperon->save();

            $message = "Thank you for updating their details!";
        } else {
            UserChaperon::Create([
                'user_id'             => auth()->user()->id,
                'name'                => $request->name,
                'email'               => $request->email,
                'menu_choice'         => isset($request->menu_choice) ? $request->menu_choice : null,
                'menu_comment'        => isset($request->menu_comment) ? $request->menu_comment : null,
                'wants_accommodation' => isset($request->wants_accommodation) ? $request->wants_accommodation : null
            ]);

            $message = "Thank you for the information, we'll keep you both updated!";
        }

        session()->flash('success', $message);

        return redirect()->action('Guest\ChaperonRsvpController@index', [$wedding]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, User $user)
    {
        $bringing = isset($request->bringingChaperon);

        $user->bringing_chaperon = $bringing;
        $user->save();

        if ($bringing) {
            $message = "Tell us more of your accomplice!";
        } else {
            if ($user->chaperon() != null) {
                UserChaperon::where('user_id', $user->id)->delete();

                debug('[User] Removed chaperon from user.');
            } else {
                debug('[User] No Chaperon to remove.');
            }

            $message = "See you there! Feel free to change your mind!";
        }

        session()->flash('success', $message);

        return redirect()->action('Guest\ChaperonRsvpController@index', [$wedding]);
    }
}
