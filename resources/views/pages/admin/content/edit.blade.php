@extends('layouts.app')
@section('title', 'Edit Page')

@section('content')
    <h1>Edit Page</h1>
    
    {!! Form::open(['action' => ['Admin\ContentController@update', $wedding->id, $content->id], 'method' => 'PUT']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $content->name, ['class' => 'form-control', 'placeholder' => 'Normal', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', $content->description, ['class' => 'form-control', 'placeholder' => 'A page to hold the itinerary for the day!', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('content', 'Page Content')}}
            {{Form::text('content', $content->content, ['id' => 'content-editor', 'class' => 'form-control', 'rows' => 5, 'required'])}}
        </div>

        {{Form::submit('Update Page', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}/content/{{$content->slug}}" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}

    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        window.onload = function(){
            setTimeout(function(){
                CKEDITOR.replace('content-editor');
            },0);
        }
    </script>
@endsection