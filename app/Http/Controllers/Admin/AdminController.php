<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Wedding;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser');
    }

    public function index()
    {
        $weddings = Wedding::all();

        return view('pages.admin.dashboard')
                ->with('weddings', $weddings);
    }
}
