@extends('layouts.app')
@section('title', 'View Page')

@section('content')
    @include('inc.nav.weddingManagement')

    <h2>Manage Website Content</h2>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><strong>Name:</strong> {{$content->name}}</a></h5>
            <p><strong>Slug:</strong> {{$content->slug}}</p>
            <p><strong>Description:</strong> {{$content->description}}</p>
            <p><strong>Content:</strong>
                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <p>{!! $content->content !!}</p>
                        </div>
                    </div>
                </div>
            </p>
        </div>
    </div
    >
    <br/>

    {!! Form::open(['action' => ['Admin\ContentController@destroy', $wedding->slug, $content->slug], 'method' => 'POST']) !!}
        <a href="/admin/wedding/{{$wedding->slug}}/content/{{$content->slug}}/edit" class="btn btn-primary">Edit</a>

        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}

        <a href="/admin/wedding/{{$wedding->slug}}/content" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}    
@endsection