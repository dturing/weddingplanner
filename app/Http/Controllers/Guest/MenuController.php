<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Wedding;
use App\Menu;
use App\User;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser|Guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function index(Wedding $wedding)
    {
        return view('pages.guest.rsvp')
                ->with(compact('wedding'))
                ->with('user', auth()->user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  User     $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, User $user)
    {
        debug('[RSVP] Updating menu choice for user.');

        $user->menu_choice = $request->menu_choice;
        $user->menu_comment = $request->menu_comment;
        $user->save();

        $menu = new Menu;
        $menu = $menu->fetchMenu($user->menu_choice);

        session()->flash('success', "You're down for the " . $menu->name . " menu! Feel free to change your mind!");

        return redirect()->action('Guest\MenuController@index', [$wedding]);
    }
}
