<?php

namespace App\Http\Middleware;

use Closure;
use Mail;
use App\Mail\SecurityWarning;
use App\User;

class CheckWedding
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if($user->isSuperUser()) return $next($request);

        if (isset($request->wedding))
        {
            $requestedWedding = $request->wedding;

            if ($requestedWedding->users()->where('id', $user->id)->count() == 0)
            {
                Mail::to('daniel.osborne90@gmail.com')->send(new SecurityWarning($user, $requestedWedding));

                session()->flash('warning', 'You are not a guest at the requested wedding. The Administrator has been made aware of this action.');

                return redirect('/home');
            }
        }

        return $next($request);
    }
}
