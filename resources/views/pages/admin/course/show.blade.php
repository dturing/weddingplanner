@extends('layouts.app')
@section('title', 'Course Details')

@section('content')
    @include('inc.nav.weddingManagement')
    <div class="card">
        <div class="card-body">
            <h3 class="card-title">Menu Course Details<h3>
            <h6><strong>Name:</strong> {{$course->name}}</h6>
            <h6><strong>Description:</strong> {{$course->description}}</h6>
            <h6><strong>Food:</strong><h6>
            @if(count($foods) > 0)
                <ul>
                    @foreach($foods as $item)
                        <li><a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course/{{$course->slug}}/food/{{$item->slug}}/edit">{{$item->name}}</a> - {{$item->description}}
                            <ul>
                                <li>Vegetarian: {{$item->vegetarian}}</li>
                                <li>Vegan: {{$item->vegan}}</li>
                                <li>Contains Nuts: {{$item->contains_nuts}}</li>
                                <li>Contains Dairy: {{$item->contains_dairy}}</li>
                                <li>Gluten Free: {{$item->gluten_free}}</li>
                            </ul>
                        </li>
                    @endforeach
                </ul>
            @else
                <h6>No food configured against this course.</h6>
            @endif
            <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course/{{$course->slug}}/food/create" class="btn btn-success">Add A Food</a>
        </div>
    </div>
    <br/>
    
    {!! Form::open(['action' => ['Admin\CourseController@destroy', $wedding->id, $menu->id, $course->id], 'method' => 'POST']) !!}
        <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course/{{$course->slug}}/edit" class="btn btn-primary">Edit</a>

        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}

        <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection