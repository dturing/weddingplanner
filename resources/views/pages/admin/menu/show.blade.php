@extends('layouts.app')
@section('title', 'View Menu')

@section('content')
    @include('inc.nav.weddingManagement')
    <div class="card">
        <div class="card-body">
            <h3 class="card-title">Wedding Menu Details<h3>

            <h6><strong>Name:</strong> {{$menu->name}}</h6>
            <h6><strong>Description:</strong> {{$menu->description}}</h6>
            <h6><strong>Courses:</strong><h6>
            @if(count($courses) > 0)
                <ul>
                    @foreach($courses as $course)
                        <li>{{$course->name}} - {{$course->description}}</li>
                    @endforeach
                </ul>
            @else
                <h6>No courses against this menu.</h6>
            @endif
            <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course">Manage Courses</a>
        </div>
    </div>
    <br/>
    
    {!! Form::open(['action' => ['Admin\MenuController@destroy', $wedding->id, $menu->id], 'method' => 'POST']) !!}
        <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/edit" class="btn btn-primary">Edit</a>
   
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}

        <a href="/admin/wedding/{{$wedding->slug}}/menu" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection