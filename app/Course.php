<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Http\Request;

class Course extends Model
{
    use HasSlug;

    public $timestamps = true;

    protected $fillable = [
        'menu_id',
        'position',
        'name',
        'slug',
        'description'
    ];

    /**
     * Create slug automatically.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Use either slug or ID to find the record.
     */
    public function getRouteKeyName(): string
    {
        $identifier = Route::current()->parameters()['course'];

        if (!ctype_digit($identifier)) {
            return 'slug';
        }

        return 'id';
    }

    // Relationships
    // All courses are owned by a Menu.
    public function course() {
        return $this->belongsTo(Menu::class);
    }

    // Each Course has many Foods.
    public function foods() {
        return $this->hasMany(Food::class);
    }

    /**
     * @param   Request     $request
     * @return  Food
     */
    public function upsertFood(Request $request)
    {
        return $this->foods()->updateOrCreate(
            [
                'id'             => $request->food_id
            ],
            [
                'name'           => $request->name,
                'description'    => $request->description,
                'vegetarian'     => $request->vegetarian     != null ?true : false,
                'vegan'          => $request->vegan          != null ?true : false,
                'contains_nuts'  => $request->contains_nuts  != null ?true : false,
                'contains_dairy' => $request->contains_dairy != null ?true : false,
                'gluten_free'    => $request->gluten_free    != null ?true : false
            ]
        );
    }
}
