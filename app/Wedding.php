<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Http\Request;
use App\Menu;

class Wedding extends Model
{
    use HasSlug;

    public $timestamps = true;

    protected $fillable = [
        'name',
        'slug',
        'when',
        'accommodation_available'
    ];

    /**
     * Create slug automatically.
     */
    public function getSlugOptions():SlugOptions {
        return SlugOptions::create()
                ->generateSlugsFrom('name')
                ->saveSlugsTo('slug');
    }

    /**
     * Use either slug or ID to find the record.
     */
    public function getRouteKeyName(): string
    {
        $identifier = Route::current()->parameters()['wedding'];

        if (!ctype_digit($identifier)) {
            return 'slug';
        }

        return 'id';
    }

    /**
     * Each wedding can have many menus.
     */
    public function menus() {
        debug('[Wedding] Fetching all menus wedding owns.');

        return $this->hasMany(Menu::class);
    }

    /**
     * Each wedding can have multiple pieces of content.
     */
    public function contents()
    {
        debug('[Wedding] Fetching all content the wedding owns.');

        return $this->hasMany(Content::class);
    }

    /**
     * Each wedding can have many users.
     */
    public function users() {
        debug('[Wedding] Fetching all users wedding owns.');

        $users = $this->belongsToMany(User::class,'user_wedding')->get();
        
        return $users;
    }

    /**
     * Returns all users that have not responded to the RSVP.
     */
    public function noRsvp() {
        debug('[Wedding] Fetching all users who have not responded to RSVP.');

        return $this->users()->where('attending', null);
    }

    /**
     * Returns all users that have confirmed they are attending.
     */
    public function attending() {
        debug('[Wedding] Fetching all users that are attending this wedding.');

        return $this->users()->where('attending', true);
    }

    /**
     * Returns all users that have not confirmed they are attending.
     */
    public function notAttending() {
        debug('[Wedding] Fetching all none attending users.');

        return $this->users()->where('attending', false);
    }

    /**
     * Returns Yes/No as to whether the wedding offers accommodation.
     */
    public function hasAccommodation() {
        debug('[Wedding] Checking whether the wedding offers accommodation.');

        return $this->accommodation_available ? 'Yes' : 'No';
    }

    /**
     * Returns all users wanting accommodation.
     */
    public function wantsAccommodation() {
        debug('[Wedding] Returns all users wanting accommodation.');

        return $this->users()->where('wants_accommodation', true);
    }

    /** 
     * Fetch a single wedding on id 
     */
    public function fetchWedding($id)
    {
        debug('[Wedding] Fetching a wedding with id of ' . $id);

        return Wedding::where('id', $id)->first();
    }

    /**
     * Fetches all users that have not chosen a menu yet.
     */
    public function noMenuChosen()
    {
        return $this->users()->where('menu_choice', null);
    }

    /**
     * Create a menu if one does not exist with the given ID.
     * Updates the menu if it already exists.
     * @param   Request  $request
     * @return  Menu     $menu
     */
    public function upsertMenu(Request $request)
    {
        debug('[Menu] Upserting menu.');

        return $this->menus()->updateOrCreate(
            [
                'id'          => $request->menu_id
            ],
            [
                'name'        => $request->name,
                'description' => $request->description
            ]
        );
    }

    /**
     * Create a wedding if one does not exist with the given ID.
     * Updates the wedding if it already exists.
     * 
     * @param   Request  $request
     * @return  Wedding  $wedding
     */
    public function upsertWedding(Request $request) {
        if (isset($request->wedding_id)) {
            debug('[User] Updating existing wedding.');

            // Mark all of the guests as not wanting accommodation
            // this stops guests from being marked as yes if the
            // accommodation terms change.
            if(!isset($request->accommodation_available))
            {
                foreach ($this->users() as $user) {
                    $user->wants_accommodation = false;
                }
            }
        } else {
            debug('[User] Creating a new wedding.');
        }
        
        return $this->updateOrCreate(
            [
                'id'                      => isset($request->wedding_id) ? $request->wedding_id : -1
            ],
            [
                'name'                    => $request->name,
                'when'                    => $request->when,
                'accommodation_available' => isset($request->accommodation_available)
            ]
        );
    }
}
