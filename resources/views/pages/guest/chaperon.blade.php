@extends('layouts.app')
@section('title', 'Plus-one')

@section('content')
    @if ($user->bringingChaperon() == 'Yes')
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Tell us a little about your accomplice.</h5>
                <p class="card-text">
                    {!! Form::open(['action' => ['Guest\ChaperonRsvpController@create', $wedding->slug, $user->slug], 'method' => 'POST']) !!}
                        <div class="form-group">
                            {{Form::label('name', 'Name')}}
                            {{Form::text('name', isset($chaperon) ? $chaperon->name : "", ['id' => 'content-editor', 'class' => 'form-control', 'required'])}}
                        </div>
                        
                        <div class="form-group">
                            {{Form::label('email', 'Email')}}
                            {{Form::text('email', isset($chaperon) ? $chaperon->email : "", ['id' => 'content-editor', 'class' => 'form-control', 'required'])}}
                        </div>

                        <div class="form-group">
                            {{Form::label('chaperonMenuChoice', 'What will they be eating?')}}
                            @if (count($wedding->menus) > 0)
                                @foreach($wedding->menus as $menu)
                                    <div class="form-group">
                                        {{Form::radio('menu_choice', $menu->id, isset($chaperon) ? $chaperon->menu_choice == $menu->id : false)}}
                                        {{Form::label('lblFoodChoice', $menu->name)}}
                                    </div>
                                    <hr>
                                @endforeach
                            @else
                                <p>The food choices haven't been decided yet, check back soon!</p>
                            @endif
                        </div>

                        <div class="form-group">
                            {{Form::label('menu_comment', 'Questions')}}
                            {{Form::textarea('menu_comment', isset($chaperon) ? $chaperon->menu_comment : "", ['id' => 'content-editor', 'class' => 'form-control', 'rows' => 3, 'required'])}}
                        </div>

                        @if ($wedding->accommodation_available)
                            {{Form::label('chaperon_accommodation', 'Will your guest be staying?')}}

                            <div class="form-group">
                                {{Form::Radio('wants_accommodation', true, isset($chaperon) ? $chaperon->wantsAccommodation() == 'Yes' : false) }}
                                {{Form::label('wants_accommodation', 'They would like to stay!')}}
                            </div>

                            <div class="form-group">
                                {{Form::Radio('wants_accommodation', false, isset($chaperon) ? $chaperon->wantsAccommodation() == 'No' : false) }}
                                {{Form::label('wants_accommodation', "They won't be staying.")}}
                            </div>
                        @endif

                        {{Form::hidden("user_id", $user->id)}}
                        {{Form::hidden("chaperon_id", isset($chaperon) ?$user->chaperon()->id : -1)}}
                        {{Form::submit('Submit Their Choices!', ['class' => 'btn btn-primary'])}}
                    {!! Form::close()!!}
                </p>
            </div>
        </div>
        <br/>
    @endif

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Will you be bringing some with you?</h5>
            <p class="card-text">
                {!! Form::open(['action' => ['Guest\ChaperonRsvpController@update', $wedding->slug, $user->slug], 'method' => 'PUT']) !!}

                    <div class="form-group">
                        {{Form::Radio('bringingChaperon', true, $user->bringingChaperon() == 'Yes') }}
                        {{Form::label('lblAttending', 'Bringing an accomplice!')}}
                    </div>

                    <div class="form-group">
                        {{Form::Radio('bringingChaperon', false, $user->bringingChaperon() == 'No') }}
                        {{Form::label('lblNotAttending', "I'm a lone wolf 🌘")}}
                    </div>
                    
                    {{Form::submit('Send your RSVP!', ['class' => 'btn btn-primary'])}}
                {!! Form::close() !!}
            </p>
        </div>
    </div>
@endsection