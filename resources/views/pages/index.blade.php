@extends('layouts.app')

@section('content')
	<div class="jumbotron text-center">
		<h1>Welcome Visitor</h1>
		<p>If you are a guest or organiser of an upcoming wedding, please login to continue.
		<p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a></p>
	</div>
@endsection	