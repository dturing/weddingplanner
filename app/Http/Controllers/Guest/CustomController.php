<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Wedding;
use App\Content;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CustomController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser|Guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Wedding  $wedding
     * @param  string   $slug
     * @return \Illuminate\Http\Response
     */
    public function index(Wedding $wedding, $slug)
    {
        try {
            $content = new Content;
            $content = Content::where('slug', $slug)->firstOrFail();

            return view('pages.guest.custom')
                ->with(compact('wedding'))
                ->with(compact('content'));
        }
        catch(ModelNotFoundException $e) {
            return view('pages.guest.index')
                    ->with(compact('wedding'))
                    ->with('warning', 'The request page was not found!');
        }
    }
}
