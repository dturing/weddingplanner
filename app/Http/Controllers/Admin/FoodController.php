<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Wedding;
use App\Menu;
use App\Course;
use App\Food;

class FoodController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser');
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @param  Course   $course
     * @return \Illuminate\Http\Response
     */
    public function create(Wedding $wedding, Menu $menu, Course $course)
    {
        return view('pages.admin.food.create')
                ->with(compact('wedding'))
                ->with(compact('menu'))
                ->with(compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @param  Course   $course
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Wedding $wedding, Menu $menu, Course $course)
    {
        $this->validate(request(), [
            'name'           => 'required|min:1|max:100',
            'description'    => 'required|min:1|max:100',
        ]);

        $food = $course->upsertFood($request);

        session()->flash('success', 'Added "' . $food->name . '" to the "' . $course->name . '" course.');

        return redirect()->action('Admin\CourseController@show', [$wedding, $menu, $course])
                ->with('foods', $course->foods);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @param  Course   $course
     * @param  Food     $food
     * @return \Illuminate\Http\Response
     */
    public function edit(Wedding $wedding, Menu $menu, Course $course, Food $food)
    {
        return view('pages.admin.food.edit')
                ->with(compact('wedding'))
                ->with(compact('menu'))
                ->with(compact('course'))
                ->with(compact('food'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @param  Course   $course
     * @param  Food     $food
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, Menu $menu, Course $course, Food $food)
    {
        $this->validate(request(), [
            'name'           => 'required|min:1|max:100',
            'description'    => 'required|min:1|max:100',
        ]);
        
        $course->upsertFood($request);

        session()->flash('success', 'Updated "' . $food->name . '" details.');

        return redirect()->action('Admin\CourseController@show', [$wedding, $menu, $course])
                ->with('foods', $course->foods);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Wedding  $wedding
     * @param  Menu     $menu
     * @param  Course   $course
     * @param  Food     $food
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wedding $wedding, Menu $menu, Course $course, Food $food)
    {
        $food->Delete();

        session()->flash('success', 'Removed "' . $food->name . '" from the "' . $course->name . '" course.');

        return redirect()->action('Admin\CourseController@show', [$wedding, $menu, $course])
                ->with('foods', $course->foods);
    }
}
