<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use App\Wedding;

class Content extends Model
{
    use HasSlug;

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wedding_id',
        'name',
        'slug',
        'description',
        'content'
    ];

    /**
     * Create slug automatically.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Use either slug or ID to find the record.
     */
    public function getRouteKeyName(): string
    {
        $identifier = Route::current()->parameters()['content'];

        if (!ctype_digit($identifier)) {
            return 'slug';
        }

        return 'id';
    }

    // Relationships
    // Each content page belongs to a wedding.
    public function wedding()
    {
        return $this->belongsTo(Wedding::class);
    }
}
