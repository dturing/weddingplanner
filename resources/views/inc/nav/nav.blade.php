<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Wedding Planner') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Navbar for logged in users -->
            <ul class="navbar-nav mr-auto">
            @auth
                @if (isset($wedding) && !Auth::User()->isSuperUser()) 
                    <a class="nav-link" href="/home">Home</a>

                    @if($wedding->accommodation_available)
                        <a class="nav-link" href="/{{$wedding->slug}}/accommodation">Accommodation</a>
                    @endif

                    <a class="nav-link" href="/{{$wedding->slug}}/rsvp">RSVP</a>

                    @if(Auth::User()->allowed_chaperon)
                        <a class="nav-link" href="/{{$wedding->slug}}/chaperon">Plus-one</a>
                    @endif

                    <!-- Custom pages -->
                    @if($wedding->contents)
                        @foreach($wedding->contents as $content)
                            <a class="nav-link" href="/{{$wedding->slug}}/page/{{$content->slug}}">{{$content->name}}</a>
                        @endforeach
                    @endif

                @elseif (Auth::User()->isSuperUser())
                    <a class="nav-link" href="/admin/wedding">All Weddings</a>
                @endif
            @endauth
            </ul>

            <!-- Logout option only -->
            <ul class="navbar-nav ml-auto">
                @auth
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::User()->name}}</a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        @if (Auth::User()->hasAnyRole(['SuperUser','Organiser']))
                            @if (Auth::User()->isSuperUser())
                                <a class="nav-link" href="/admin">Site Admin</a>
                            @else
                                <a class="nav-link" href="/admin/wedding/{{$wedding->slug}}">Organiser</a>
                            @endif
                            
                            <div class="dropdown-divider"></div>
                        @endif

                        <a class="nav-link" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>  
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endauth

                @guest
                    <a class="nav-link" href="/login">Login</a>
                @endguest
            </ul>
        </div>
    </div>
</nav>
</p>