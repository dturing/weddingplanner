@extends('layouts.app')
@section('title', 'Create Page')

@section('content')
    <h1>Create Page</h1>

    {!! Form::open(['action' => ['Admin\ContentController@store', $wedding->id], 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Normal', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', '', ['class' => 'form-control', 'placeholder' => 'The default menu with no special dietary requirements.', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('content', 'Page Content')}}
            {{Form::textarea('content', '', ['id' => 'content-editor', 'class' => 'form-control', 'required'])}}
        </div>

        {{Form::submit('Create Page', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}/content" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}

    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        window.onload = function(){
            setTimeout(function(){
                CKEDITOR.replace('content-editor');
            },0);
        }
    </script>
@endsection