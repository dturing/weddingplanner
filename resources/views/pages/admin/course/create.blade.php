@extends('layouts.app')
@section('title', 'Create Course')

@section('content')
    <h1>Create Course</h1>

    {!! Form::open(['action' => ['Admin\CourseController@store', $wedding->id, $menu->id], 'method' => 'POST']) !!}

        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Starter', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', '', ['class' => 'form-control', 'placeholder' => 'A starter dish to get ready for the main!.', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('position', 'Position')}}
            {{Form::number('position', '', ['class' => 'form-control', 'placeholder' => '1', 'required'])}}
        </div>

        {{Form::submit('Create Course', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}/course" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection