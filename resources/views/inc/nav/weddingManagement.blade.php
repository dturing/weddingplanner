<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-brand">Manage</div>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="/admin/wedding/{{$wedding->slug}}">Overview</span></a>
            <a class="nav-item nav-link" href="/admin/wedding/{{$wedding->slug}}/user">Guests</a>
            <a class="nav-item nav-link" href="/admin/wedding/{{$wedding->slug}}/menu">Catering</a>
            <a class="nav-item nav-link" href="/admin/wedding/{{$wedding->slug}}/content">Content Editor</a>
        </div>
    </div>
</nav>