@extends('layouts.app')
@section('title', 'Edit Dish')

@section('content')
    <h1>Edit Dish Details</h1>
    
    {!! Form::open(['action' => ['Admin\FoodController@update', $wedding->id, $menu->id, $course->id, $food->id], 'method' => 'put']) !!}
    {{Form::hidden('food_id', $food->id)}}

    <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $food->name, ['class' => 'form-control', 'placeholder' => 'Name of the dish', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', $food->description, ['class' => 'form-control', 'placeholder' => 'A tasty treat for anyone.', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('vegetarian', 'Vegetarian')}}
            {{Form::checkbox('vegetarian', null, $food->vegetarian)}}
        </div>

        <div class="form-group">
            {{Form::label('vegan', 'Vegan')}}
            {{Form::checkbox('vegan', null, $food->vegan)}}
        </div>

        <div class="form-group">
            {{Form::label('contains_nuts', 'Contains Nuts')}}
            {{Form::checkbox('contains_nuts', null, $food->contains_nuts)}}
        </div>

        <div class="form-group">
            {{Form::label('contains_dairy', 'Contains Dairy')}}
            {{Form::checkbox('contains_dairy', null, $food->contains_dairy)}}
        </div>

        <div class="form-group">
            {{Form::label('gluten_free', 'Gluten Free')}}
            {{Form::checkbox('gluten_free', null, $food->gluten_free)}}
        </div>

        {{Form::submit('Update Food', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
        <br/>
    {!! Form::open(['action' => ['Admin\FoodController@destroy', $wedding->id, $menu->id, $course->id, $food->id], 'method' => 'POST']) !!}

        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Remove Item', ['class' => 'btn btn-danger']) }}

        <a href="/admin/wedding/{{$wedding->id}}/menu/{{$menu->id}}/course/{{$course->id}}" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection