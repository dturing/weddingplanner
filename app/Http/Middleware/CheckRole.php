<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    { 
        if ($request->user() == null) {
            return redirect('/');
        }

        $roles = explode('|', $roles);

        if ($request->user()->hasAnyRole($roles) || !$roles) {
            return $next($request);
        }

        return redirect('/home')
                ->with('error','You do not have permission to access the request page.');
    }
}
