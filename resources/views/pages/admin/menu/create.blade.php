@extends('layouts.app')
@section('title', 'Create Menu')

@section('content')
    <h1>Create New Wedding Menu</h1>

    {!! Form::open(['action' => ['Admin\MenuController@store', $wedding->id], 'method' => 'POST']) !!}
        
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Normal', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', '', ['class' => 'form-control', 'placeholder' => 'The default menu with no special dietary requirements.', 'required'])}}
        </div>

        {{Form::hidden('wedding_id', $wedding->id)}}
        {{Form::submit('Create Menu', ['class' => 'btn btn-primary'])}}

        <a href="/admin/wedding/{{$wedding->slug}}/menu" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection