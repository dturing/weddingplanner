<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Wedding;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser|Guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isSuperUser()) {
            return view('pages.admin.dashboard')
                    ->with('weddings', Wedding::all());
        }

        $wedding = auth()->user()->fetchWeddings();

        return view('pages.guest.index')
                ->with(compact('wedding'));
    }

    public function accommodation(Wedding $wedding)
    {
        return view('pages.guest.accommodation')
                ->with(compact('wedding'));
    }

    public function registry(Wedding $wedding)
    {
        return view('pages.guest.registry')
            ->with(compact('wedding'));
    }

    public function schedule(Wedding $wedding)
    {
        return view('pages.guest.schedule')
            ->with(compact('wedding'));
    }

    public function travel(Wedding $wedding)
    {
        return view('pages.guest.travel')
            ->with(compact('wedding'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
