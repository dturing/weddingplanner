<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;
use App\User;

class VisitorController extends Controller
{
    public function index() {
    	if (auth()->check()) {
    		return redirect('/home');
   	    } else {
        	return view('pages.index');
        }
    }
}
