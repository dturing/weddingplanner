<h2>Security Warning</h2>

<p>A user tried to access a wedding that they are not a guest of.</p>

<strong>User:</strong>
<p>{{$user}}</p>

<br/>

<strong>Requested Wedding:</strong>
<p>{{$requestedWedding}}</p>