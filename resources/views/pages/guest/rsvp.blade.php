@extends('layouts.app')
@section('title', 'RSVP & Menu')

@section('content')
        @if ($user->attending)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">What Would You Like To Eat?</h5>
                    <p class="card-text">                    
                        @if (count($wedding->menus) > 0)
                            {!! Form::open(['action' => ['Guest\MenuController@update', $wedding->slug, $user->slug], 'method' => 'PUT']) !!}
                                @foreach ($wedding->menus as $menu)
                                    <div class="form-group">
                                        {{Form::radio('menu_choice', $menu->id, $user->menu_choice == $menu->id)}}
                                        {{Form::label('lblFoodChoice', $menu->name)}}

                                    @if(count($menu->courses) > 0)
                                        <p>Courses:</p>
                                        <ul>
                                            @foreach ($menu->courses as $course)
                                                <li>{{$course->name}} - {{$course->description}}</li>
                                                <ul>
                                                @foreach($course->foods as $food)
                                                    <li>{{$food->name}} - {{$food->description}}</li>
                                                @endforeach
                                                </ul>
                                            @endforeach
                                        </ul>
                                    @endif
                                    </div>
                                    <hr>
                                @endforeach

                                <div class="form-group">
                                    {{Form::label('lblMenuComment', 'Questions')}}
                                    {{Form::textarea('menu_comment', $user->menu_comment, ['id' => 'content-editor', 'class' => 'form-control', 'rows' => 3, 'required'])}}
                                </div>

                                {{Form::submit('Submit Your Choice!', ['class' => 'btn btn-primary'])}}
                            {!! Form::close()!!}
                        @else
                            The food choices haven't been decided yet, check back soon!
                        @endif
                    </p>
                </div>
            </div>
            <br/>
        @endif
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Will You be Joining Us?</h5>
                <p class="card-text">
                    {!! Form::open(['action' => ['Guest\RsvpController@update', $wedding->slug, $user->slug], 'method' => 'PUT']) !!}

                        <div class="form-check">
                            {{Form::Radio('attending', true, $user->isAttending() == 'Yes') }}
                            {{Form::label('lblAttending', 'Attend With Excitment!')}}
                        </div>

                        <div class="form-check">
                            {{Form::Radio('attending', false, $user->isAttending() == 'No') }}
                            {{Form::label('lblNotAttending', "Can't make it, sorry!")}}
                        </div>
                        
                        {{Form::submit('Send your RSVP!', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}
                </p>
            </div>
        </div>
    </div>
@endsection	