<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Wedding;
use Auth;

class WeddingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $weddings = Auth::user()->fetchWeddings();

        return view('pages.admin.wedding.index')
                ->with(compact('weddings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('pages.admin.wedding.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|min:1|max:100',
            'when' => 'required|date|after:today',
        ]);

        $wedding = new Wedding;
        $wedding = $wedding->upsertWedding($request);

        session()->flash('success', 'Wedding "' . $wedding->name . '" Created');

        return redirect()->action('Admin\WeddingController@index')
                ->with('weddings', Wedding::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function show(Wedding $wedding)
    {
        return view('pages.admin.wedding.show')
                ->with(compact('wedding'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function edit(Wedding $wedding)
    {
        return view('pages.admin.wedding.edit')
                ->with(compact('wedding'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding)
    {
        $this->validate(request(), [
            'name' => 'required|min:1|max:100',
            'when' => 'required|date|after:today',
        ]);

        $wedding = new Wedding;
        $wedding = $wedding->upsertWedding($request);

        session()->flash('success', 'Wedding "' . $wedding->name . '" Updated');

        return redirect()->action('Admin\WeddingController@show', [$wedding]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wedding $wedding)
    {
        $wedding->users()->delete();
        $wedding->delete();

        session()->flash('success', 'Wedding "' . $wedding->name . '" Removed');

        return redirect()->action('Admin\WeddingController@index')
                ->with('weddings', Wedding::all());
    }
}
