@extends('layouts.app')
@section('title', 'Accommodation')

@section('content')
    <div class="card">
    <div class="card-body">
        <h5 class="card-title">Will You be Staying With Us?</h5>
        <p class="card-text">
            {!! Form::open(['action' => ['Guest\AccommodationController@update', $wedding->slug, $user->slug], 'method' => 'PUT']) !!}

                <div class="form-check">
                    {{Form::Radio('wants_accommodation', true, $user->wantsAccommodation() == 'Yes') }}
                    {{Form::label('lblStaying', 'Yes Please!')}}
                </div>

                <div class="form-check">
                    {{Form::Radio('wants_accommodation', false, $user->wantsAccommodation() == 'No') }}
                    {{Form::label('lblNotStaying', "No thanks!")}}
                </div>
                
                {{Form::submit('Let Us Know!', ['class' => 'btn btn-primary'])}}
            {!! Form::close() !!}
        </p>
    </div>
</div>
@endsection	