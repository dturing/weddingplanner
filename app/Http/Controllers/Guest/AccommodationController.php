<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Wedding;
use App\User;

class AccommodationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser|Guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Wedding $wedding)
    {
        if (!$wedding->accommodation_available) {
            return view('pages.guest.index')
                    ->with(compact('wedding'))
                    ->with('warning', "The Wedding you're attending doesn't offer accommodation, sorry!");
        }

        return view('pages.guest.accommodation')
            ->with(compact('wedding'))
            ->with('user', auth()->user());       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  User     $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, User $user)
    {
        debug('[Accommodation] Updating accommodation preference for user.');

        $user->wants_accommodation = isset($request->wants_accommodation);
        $user->save();

        $message = "Let us know if anything changes!";

        if ($user->wants_accommodation) {
            $message = "We'll be in touch!";
        }

        session()->flash('success', $message);

        return redirect()->action('Guest\AccommodationController@index', [$wedding]);
    }
}
