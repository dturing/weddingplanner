<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Route;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Role;
use App\Wedding;
use App\Menu;

class User extends Authenticatable 
{
    use Notifiable;
    use HasSlug;

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'slug',
        'email',
        'password',
        'attending',
        'wants_accommodation',
        'allowed_plus_one',
        'menu_choice',
        'menu_comment',
        'allowed_chaperon',
        'chaperon_id'
    ];

    /**
     * Create slug automatically.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Use either slug or ID to find the record.
     */
    public function getRouteKeyName(): string
    {
        $identifier = Route::current()->parameters()['user'];

        if (!ctype_digit($identifier)) {
            return 'slug';
        }

        return 'id';
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token'
    ];  

    /**
     * Each user can have many roles.
     */
    public function roles() {
        debug('[User] Fetching all roles user has assigned.');

        return $this->belongsToMany(Role::class, 'user_role');
    }

    /**
     * Many users belong to a wedding.
     */
    public function weddings() {
        return $this->belongsToMany(Wedding::class, 'user_wedding');
    }

    /** 
     * A user will have a menu choice.
     */
    public function menuChoice() {
        $menu = new Menu;
        $menu = Menu::where('id', $this->menu_choice)->first();

        if ($menu == null) {
            return 'Not yet chosen';
        }

        return $menu->name;
    }

    /**
     * Each user is attached to a wedding.
     */
    public function fetchWeddings() {
        debug('[User] Fetching all Weddings user has access to.');

        if ($this->hasRole('SuperUser')) {
            $weddings = Wedding::all();
        } else {
            $weddings = $this->belongsToMany(Wedding::class, 'user_wedding')->first();
        }

        return $weddings;
    }

    /**
     * Returns Yes/No depending on whether the user is attending.
     */
    public function isAttending() {
        if (isset($this->attending)) {
            return $this->attending ? 'Yes' : 'No';
        } else {
            return 'Not responded';
        }
    }

    /**
     * Returns Yes/No depending on whether the user wants accommodation.
     */
    public function wantsAccommodation() {
        if (isset($this->wants_accommodation)) {
            return $this->wants_accommodation ? 'Yes' : 'No';
        } else {
            return 'Not responded';
        }
    }

    /**
     * Returns true if the user is a super user 
     * and false if not.
     */
    public function isSuperUser() {
        return $this->hasRole('SuperUser') !== null;
    }

    /**
     * Checks if the user has any of the given roles.
     * Returns: 
     *   TRUE  If user has at least one of the roles.
     *   FALSE If the user does not have at least one of the roles.
     */
    public function hasAnyRole($roles) {
        debug('[User] Checking user has one of multiple roles.');

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the user has the given role.
     * Returns:
     *   TRUE  If the user have the given role.
     *   False If the user does not have the given role.
     */
    public function hasRole($role) {
        $hasRole = $this->roles()->where('name', $role)->first();
        
        debug('[User] Checking user has role "' . $role . '"');

        debug($hasRole
                ? '>> Has the role.'
                : '>> Does not have the role.');

        return $hasRole;
    }

    /**
     * Returns a True/False as to whether the guest has a chaperon.
     */
    public function bringingChaperon() {
        if (isset($this->bringing_chaperon)) {
            return $this->bringing_chaperon ? 'Yes' : 'No';
        } else {
            return null;
        }
    }

    /**
     * Returns the users chaperon, if the user does not have one it returns null.
     */
    public function chaperon() {
        debug('[User] Fetching chaperon for user.');
        
        $chaperon = $this->hasOne(UserChaperon::class)->first();

        return $chaperon; 
    }
    
    /**
     * Creates the user if a user with the given ID does not already exist.
     * Updates the user with the new details if the user already exists.
     * 
     * @param   Request  $request
     * @return  User
     */
    public function upsertUser(Request $request) {
        $update = isset($request->user_id);
        $roleToApply = $request->organiser == null ? 'Guest' : 'Organiser';

        if ($update) {
            debug('[User] Updating existing user.');

            $user = User::findOrFail($request->user_id);

            $user->name = $request->name;
            $user->email = $request->email;
            $user->attending = isset($request->attending) ? True : null;
            $user->wants_accommodation = isset($request->wants_accommodation) ? True : null;
            $user->allowed_chaperon = isset($request->allowed_chaperon) ? True : False;

            $user->save();

            self::handleChaperon($user);
        } else {
            debug('[User] Creating new user.');

            $user = $this->create(
                [
                    'name'                => $request->name,
                    'email'               => $request->email,
                    'password'            => Hash::make($request->password),
                    'attending'           => isset($request->attending) ? True : null,
                    'wants_accommodation' => isset($request->wants_accommodation) ? True : null,
                    'allowed_chaperon'    => isset($request->allowed_chaperon) ? True : False
                ]);

            self::handleWedding($user, $request, $roleToApply);
        }

        self::handleRole($user, $roleToApply);

        return $user;
    }

    /**
     * Apply the appropriate role, and remove 'Organiser' if it's been revoked.
     */
    private function handleRole($user, $roleToApply)
    {
        if (!$user->hasRole($roleToApply)) {
            $user->roles()->attach(Role::where('name', $roleToApply)->first());
        } elseif ($roleToApply == 'Guest' && $user->hasRole('Organiser')) {
            $user->roles()->detach(Role::where('name', 'Organiser')->first());
        }
    }

    /**
     * Handle the wedding that the user is assigned against when they are first created.
     */
    private function handleWedding(User $user, Request $request, $roleToApply)
    {
        /* Only need to assign someone against a wedding when they are created. */
        if ($roleToApply == 'Organiser') {
            /* New accounts, if created with organiser role, need the guest role too. */
            $user->roles()->attach(Role::where('name', 'Guest')->first());
        }

        $user->weddings()->attach(Wedding::where('id', $request->wedding_id)->first());
    }

    /**
     * Remove the chaperon link from the user, as well as the chaperon record itself.
     */
    private function handleChaperon(User $user)
    {
        if (!$user->allowed_chaperon)
        {
            if (isset($user->chaperon_id))
            {
                $chaperon = User::findOrFail($user->chaperon_id);
                
                $user->chaperon_id = null;
                $user->save();
                
                $chaperon->delete();
            }
        }
    }
}
