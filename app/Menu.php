<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Http\Request;

class Menu extends Model
{
    use HasSlug;

    public $timestamps = true;

    protected $fillable = [
        'wedding_id',
        'name',
        'slug',
        'description'
    ];

    /**
     * Create slug automatically.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Use either slug or ID to find the record.
     */
    public function getRouteKeyName(): string
    {
        $identifier = Route::current()->parameters()['menu'];

        if (!ctype_digit($identifier)) {
            return 'slug';
        }

        return 'id';
    }

    /**
     * Each Menu belongs to a Wedding.
     */
    public function wedding() {
        return $this->belongsTo(Wedding::class);
    }

    /**
     * Each Menu has many Courses.
     */
    public function courses() {
        return $this->hasMany(Course::class)->orderBy('position', 'asc');
    }

    /**
     * Fetches the Menu that has the matching ID.
     */
    public function fetchMenu($id) {
        return Menu::where('id', $id)->first();
    }

    /**
     * Fetches all users that have chosen this Menu.
     */
    public function chosen() {
        return User::where('menu_choice', $this->id)->get();
    }

    /**
     * @param   Request  $request
     * @return  Course   $menuCourse
     */
    public function upsertCourse(Request $request)
    {
        return $this->courses()->updateOrCreate(
            [
                'id'          => $request->course_id
            ],
            [
                'name'        => $request->name,
                'description' => $request->description,
                'position'    => $request->position
            ]
        );
    }
}
