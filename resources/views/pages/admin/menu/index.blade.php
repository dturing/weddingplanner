@extends('layouts.app')
@section('title', 'Menus')

@section('content')
    @include('inc.nav.weddingManagement')
    <h1>Wedding Menus</h1>

    @if(count($menus) > 0)
        @foreach($menus as $menu)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}}">{{$menu->name}}</a></h5>
                    <p>{{$menu->description}}</p>
                </div>
            </div>
            <br/>
        @endforeach
    @else
        <p>No menus for this wedding were found.</p>
    @endif

    <a href="/admin/wedding/{{$wedding->slug}}/menu/create" class="btn btn-primary">Create Menu</a>
    <a href="/admin/wedding/{{$wedding->slug}}" class="btn btn-error float-right">Go Back</a>
@endsection