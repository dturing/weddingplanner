@extends('layouts.app')
@section('title', 'Guests')

@section('content')
    @include('inc.nav.weddingManagement')
    <h1>Current Guests</h1>

    @if(count($users) > 0)
        @foreach($users as $user)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><a href="/admin/wedding/{{$wedding->slug}}/user/{{$user->slug}}">{{$user->name}}</a></h5>
                    <div id="guest-email" class="guest-attribute"><strong>Email:</strong> {{$user->email}}</div>
                    <div id="guest-role" class="guest-attribute"><strong>Roles:</strong></div>
                    <ul>
                        @foreach ($user->roles as $role)
                            <li>{{$role->name}}</li>
                        @endforeach
                    </ul>
                    <div id="guest-accommodation" class="guest-attribute"><strong>Consider For Accommodation:</strong> {{$user->wantsAccommodation()}}</div>
                    <div id="guest-menu-choice" class="guest-attribute"><strong>Menu Choice:</strong> {{$user->menuChoice()}}</div>
                </div>
            </div>
            <br/>
        @endforeach
    @else
        <p>No guests found :(</p>
    @endif

    <a href="/admin/wedding/{{$wedding->slug}}/user/create" class="btn btn-primary">Create Guest</a>
    <a href="/admin/wedding/{{$wedding->slug}}" class="btn btn-error float-right">Go Back</a>
@endsection