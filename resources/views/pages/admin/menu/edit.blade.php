@extends('layouts.app')
@section('title', 'Edit Menu')

@section('content')
    <h1>Edit Wedding Menu</h1>
    
    {!! Form::open(['action' => ['Admin\MenuController@update', $wedding->id, $menu->id], 'method' => 'put']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $menu->name, ['class' => 'form-control', 'placeholder' => 'Normal', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::text('description', $menu->description, ['class' => 'form-control', 'placeholder' => 'The default menu with no special dietary requirements.', 'required'])}}
        </div>

        {{Form::hidden('menu_id', $menu->id)}}
        {{Form::hidden('wedding_id', $wedding->id)}}
        {{Form::submit('Update Menu', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding/{{$wedding->slug}}/menu/{{$menu->slug}};" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection