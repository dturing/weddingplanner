@extends('layouts.app')
@section('title', 'Dashboard')

@section('content')
	<h1></h1>
	<br/>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<div class="card-body">
						<h2 class="card-title"><a href="admin/wedding">Wedding Overview</a><h2>
						<h6>Active: {{$weddings->count()}}</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection