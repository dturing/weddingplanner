<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Wedding;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('wedding');
        $this->middleware('roles:SuperUser|Organiser');
    }

    /**
     * Display a listing of the resource.
     * 
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function index(Wedding $wedding)
    {      
        return view('pages.admin.guest.index')
                ->with(compact('wedding'))
                ->with('users', $wedding->users());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Wedding $wedding
     * @return \Illuminate\Http\Response
     */
    public function create(Wedding $wedding)
    {
        return view('pages.admin.guest.create')
                ->with(compact('wedding'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'wedding_id' => 'required',
            'name'       => 'required|min:1|max:100',
            'email'      => 'required|unique:users|email',
            'password'   => 'required|min:5|max:100'
        ]);

        $user = new User;
        $user = $user->upsertUser($request);

        $wedding = new Wedding;
        $wedding = $wedding->fetchWedding($request->wedding_id);

        session()->flash('success', 'Added ' . $user->name . ' to the guest list!');

        return redirect()->action('Admin\UserController@index', [$wedding]);
    }

    /**
     * Update the specified resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  Wedding  $wedding
     * @param  User     $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wedding $wedding, User $user)
    {
        $this->validate(request(), [
            'wedding_id' => 'required',
            'name'       => 'required|min:1|max:100',
            'email'      => 'required|email'
        ]);

        $user = $user->upsertUser($request);

        session()->flash('success', 'Updated ' . $user->name . "'s details!");

        return redirect()->action('Admin\UserController@show', [$wedding, $user]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Wedding  $wedding
     * @param  User     $user
     * @return \Illuminate\Http\Response
     */
    public function show(Wedding $wedding, User $user)
    {
        return view('pages.admin.guest.show')
                ->with(compact('wedding'))
                ->with(compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Wedding  $wedding
     * @return \Illuminate\Http\Response
     */
    public function edit(Wedding $wedding, User $user)
    {
        return view('pages.admin.guest.edit')
                ->with(compact('wedding'))
                ->with(compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Wedding  $wedding
     * @param  User     $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wedding $wedding, User $user)
    {
        $user->delete();

        session()->flash('success', 'Removed ' . $user->name . ' from the guest list.');

        return redirect()->action('Admin\UserController@index', [$wedding])
                ->with('guests', $wedding->guests);
    }
}
