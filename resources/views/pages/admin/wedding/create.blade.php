@extends('layouts.app')
@section('title', 'Add Wedding')

@section('content')
    <h1>Create New Wedding</h1>

    {!! Form::open(['action' => 'Admin\WeddingController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Our Special Day', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::label('when', 'When')}}
            {{Form::date('when', date('y-M-d'), ['class' => 'form-control', 'required'])}}
        </div>

        <div class="form-group">
            {{Form::checkbox('accommodation_available')}}
            {{Form::label('accommodation', 'Is arranged accommodation an option for your guests?')}}
        </div>

        {{Form::submit('Create Wedding', ['class' => 'btn btn-primary'])}}
        <a href="/admin/wedding" class="btn btn-error float-right">Go Back</a>
    {!! Form::close() !!}
@endsection